# 阿里云RocketMQ（ons）中间件starter

### 引入依赖

```xml
<!--引入starter依赖-->
<dependency>
    <groupId>com.paul.ons</groupId>
    <artifactId>ons-spring-boot-starter</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>
```

### 添加RocketMQ 配置信息

```properties
# 启动之前请替换如下 XXX 为您的配置
ons.rocketmq.name-server=xxx
ons.rocketmq.access-key=xxx
ons.rocketmq.secret-key=xxx
# 是否开启消费者消息消费 默认为true
ons.rocketmq.enable-consumer-listener=true
```

### 发送消息

```java
// 注入普通消息发送工具（包含多种[普通、顺序、延迟、异步]消息发送，详情见方法注释）
@Resource
private ProducerMessage producerMessage;

// 注入事务消息发送工具
@Resource
private TransactionProductMessage transactionProductMessage;

// 发送消息
producerMessage.send(topic, tag, body);

// 事务消息请查看官方文档示例
```

### 消费消息，单处理（推荐）

```java
@RocketMessageListener(topic = "xxx", tag = "xxx", group = "GID_xxx")
public class DemoListener extends BaseConsumer {

    @Override
    public void consumer(Message message) {
        // 业务逻辑
    }

}
```

### 消费消息，多处理（不推荐）

```java
@RocketMessageListener(topic = "xxx", tag = {"xxx","ooo"}, group = "GID_xxx")
public class DemoListener extends BaseConsumer {

    @Override
    public void consumer(Message message) {
        switch (message.getTag()) {
            case "xxx":
                // 业务逻辑
                break;
            case "ooo":
                // 业务逻辑
                break;
            default:
        }
    }

}
```



