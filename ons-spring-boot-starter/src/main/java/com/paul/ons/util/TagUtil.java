package com.paul.ons.util;

/**
 * TagUtil
 *
 * @author paul
 * @date 2021-12-30 14:39
 */
public class TagUtil {

    public static String getTag(String[] tags) {
        if (tags == null || tags.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < tags.length - 1; i++) {
            sb.append(tags[i]).append("||");
        }
        return sb.append(tags[tags.length - 1]).toString();
    }

}