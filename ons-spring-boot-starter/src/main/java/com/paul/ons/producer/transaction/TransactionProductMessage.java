/**
 * Copyright (c) [Year] [name of copyright holder]
 * [Software Name] is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * <p>
 * <p>
 * Mulan Permissive Software License，Version 2
 * <p>
 * Mulan Permissive Software License，Version 2 (Mulan PSL v2)
 * January 2020 http://license.coscl.org.cn/MulanPSL2
 * <p>
 * Your reproduction, use, modification and distribution of the Software shall be subject to Mulan PSL v2 (this License) with the following terms and conditions:
 * <p>
 * 0. Definition
 * <p>
 * Software means the program and related documents which are licensed under this License and comprise all Contribution(s).
 * <p>
 * Contribution means the copyrightable work licensed by a particular Contributor under this License.
 * <p>
 * Contributor means the Individual or Legal Entity who licenses its copyrightable work under this License.
 * <p>
 * Legal Entity means the entity making a Contribution and all its Affiliates.
 * <p>
 * Affiliates means entities that control, are controlled by, or are under common control with the acting entity under this License, ‘control’ means direct or indirect ownership of at least fifty percent (50%) of the voting power, capital or other securities of controlled or commonly controlled entity.
 * <p>
 * 1. Grant of Copyright License
 * <p>
 * Subject to the terms and conditions of this License, each Contributor hereby grants to you a perpetual, worldwide, royalty-free, non-exclusive, irrevocable copyright license to reproduce, use, modify, or distribute its Contribution, with modification or not.
 * <p>
 * 2. Grant of Patent License
 * <p>
 * Subject to the terms and conditions of this License, each Contributor hereby grants to you a perpetual, worldwide, royalty-free, non-exclusive, irrevocable (except for revocation under this Section) patent license to make, have made, use, offer for sale, sell, import or otherwise transfer its Contribution, where such patent license is only limited to the patent claims owned or controlled by such Contributor now or in future which will be necessarily infringed by its Contribution alone, or by combination of the Contribution with the Software to which the Contribution was contributed. The patent license shall not apply to any modification of the Contribution, and any other combination which includes the Contribution. If you or your Affiliates directly or indirectly institute patent litigation (including a cross claim or counterclaim in a litigation) or other patent enforcement activities against any individual or entity by alleging that the Software or any Contribution in it infringes patents, then any patent license granted to you under this License for the Software shall terminate as of the date such litigation or activity is filed or taken.
 * <p>
 * 3. No Trademark License
 * <p>
 * No trademark license is granted to use the trade names, trademarks, service marks, or product names of Contributor, except as required to fulfill notice requirements in Section 4.
 * <p>
 * 4. Distribution Restriction
 * <p>
 * You may distribute the Software in any medium with or without modification, whether in source or executable forms, provided that you provide recipients with a copy of this License and retain copyright, patent, trademark and disclaimer statements in the Software.
 * <p>
 * 5. Disclaimer of Warranty and Limitation of Liability
 * <p>
 * THE SOFTWARE AND CONTRIBUTION IN IT ARE PROVIDED WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED. IN NO EVENT SHALL ANY CONTRIBUTOR OR COPYRIGHT HOLDER BE LIABLE TO YOU FOR ANY DAMAGES, INCLUDING, BUT NOT LIMITED TO ANY DIRECT, OR INDIRECT, SPECIAL OR CONSEQUENTIAL DAMAGES ARISING FROM YOUR USE OR INABILITY TO USE THE SOFTWARE OR THE CONTRIBUTION IN IT, NO MATTER HOW IT’S CAUSED OR BASED ON WHICH LEGAL THEORY, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * <p>
 * 6. Language
 * <p>
 * THIS LICENSE IS WRITTEN IN BOTH CHINESE AND ENGLISH, AND THE CHINESE VERSION AND ENGLISH VERSION SHALL HAVE THE SAME LEGAL EFFECT. IN THE CASE OF DIVERGENCE BETWEEN THE CHINESE AND ENGLISH VERSIONS, THE CHINESE VERSION SHALL PREVAIL.
 * <p>
 * END OF THE TERMS AND CONDITIONS
 * <p>
 * How to Apply the Mulan Permissive Software License，Version 2 (Mulan PSL v2) to Your Software
 * <p>
 * To apply the Mulan PSL v2 to your work, for easy identification by recipients, you are suggested to complete following three steps:
 * <p>
 * i Fill in the blanks in following statement, including insert your software name, the year of the first publication of your software, and your name identified as the copyright owner;
 * <p>
 * ii Create a file named “LICENSE” which contains the whole context of this License in the first directory of your software package;
 * <p>
 * iii Attach the statement to the appropriate annotated syntax at the beginning of each source file.
 * <p>
 * <p>
 * Copyright (c) [Year] [name of copyright holder]
 * [Software Name] is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.paul.ons.producer.transaction;

import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.ONSFactory;
import com.aliyun.openservices.ons.api.SendResult;
import com.aliyun.openservices.ons.api.transaction.LocalTransactionChecker;
import com.aliyun.openservices.ons.api.transaction.LocalTransactionExecuter;
import com.aliyun.openservices.ons.api.transaction.TransactionProducer;
import com.aliyun.openservices.shade.com.alibaba.rocketmq.client.producer.TransactionCheckListener;
import com.paul.ons.producer.normal.ProducerMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.paul.ons.config.OnsAutoConfiguration;

import javax.annotation.Resource;
import java.util.concurrent.ConcurrentHashMap;

/**
 * TransactionProductMessage
 * 事务消息
 *
 * @author paul
 * @date 2020-10-10 17:56
 */
public class TransactionProductMessage {

    private final Logger log = LoggerFactory.getLogger(ProducerMessage.class);

    @Resource
    private OnsAutoConfiguration onsConfiguration;

    /**
     * 发送事务消息
     * 1.发送半事务消息到broke, broke确认后返回 send ok
     * 2.执行本地事务，执行成功返回事务状态 committed 或者 rollback
     * 3.回查本地事务状态 如果是 committed 则投递消息给消费者，反之销毁消息
     * 如果状态是 Unknow ,,0-30秒重试
     *
     * @param message  消息载体
     * @param executor 本地事务执行器
     * @param checker  回查本地事务 回查间隔时间：系统默认每隔30秒发起一次定时任务，对未提交的半事务消息进行回查，共持续12小时。
     * @param arg      应用自定义参数，该参数可以传入本地事务执行器
     * @return 发送结果
     */
    public SendResult sendMessageInTransaction(Message message, LocalTransactionExecuter executor, Object arg, LocalTransactionChecker checker) {
        SendResult sendResult;
        TransactionProducer transactionProducer = ONSFactory.createTransactionProducer(onsConfiguration.getMqProperties(), checker);
        if (!transactionProducer.isStarted()) {
            transactionProducer.start();
        }

        try {
            sendResult = transactionProducer.send(message, executor, arg);
        } catch (Exception e) {
            log.error("事务消息发送异常：message:{},body:{}", message, new String(message.getBody()));
            throw new RuntimeException();
        }
        log.debug("发送事务消息结果：message:{},body:{},sendResult:{}", message, new String(message.getBody()), sendResult);
        return sendResult;
    }

    /**
     * 发送事务消息
     * 1.发送半事务消息到broke, broke确认后返回 send ok
     * 2.执行本地事务，执行成功返回事务状态 committed 或者 rollback
     * 3.回查本地事务状态 如果是 committed 则投递消息给消费者，反之销毁消息
     * 如果状态是 Unknow ,0-30秒重试
     *
     * @param message  消息载体
     * @param executor 本地事务执行器
     * @param checker  回查本地事务 回查间隔时间：系统默认每隔30秒发起一次定时任务，对未提交的半事务消息进行回查，共持续12小时。
     * @return 发送结果
     */
    public SendResult sendMessageInTransaction(Message message, LocalTransactionExecuter executor, LocalTransactionChecker checker) {
        SendResult sendResult;
        TransactionProducer transactionProducer = ONSFactory.createTransactionProducer(onsConfiguration.getMqProperties(), checker);
        if (!transactionProducer.isStarted()) {
            transactionProducer.start();
        }

        try {
            sendResult = transactionProducer.send(message, executor, null);
        } catch (Exception e) {
            log.error("事务消息发送异常：message:{},body:{}", message, new String(message.getBody()));
            throw new RuntimeException();
        }
        log.debug("发送事务消息结果：message:{},body:{},sendResult:{}", message, new String(message.getBody()), sendResult);
        return sendResult;
    }

}
