/**
 * Copyright (c) [Year] [name of copyright holder]
 * [Software Name] is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * <p>
 * <p>
 * Mulan Permissive Software License，Version 2
 * <p>
 * Mulan Permissive Software License，Version 2 (Mulan PSL v2)
 * January 2020 http://license.coscl.org.cn/MulanPSL2
 * <p>
 * Your reproduction, use, modification and distribution of the Software shall be subject to Mulan PSL v2 (this License) with the following terms and conditions:
 * <p>
 * 0. Definition
 * <p>
 * Software means the program and related documents which are licensed under this License and comprise all Contribution(s).
 * <p>
 * Contribution means the copyrightable work licensed by a particular Contributor under this License.
 * <p>
 * Contributor means the Individual or Legal Entity who licenses its copyrightable work under this License.
 * <p>
 * Legal Entity means the entity making a Contribution and all its Affiliates.
 * <p>
 * Affiliates means entities that control, are controlled by, or are under common control with the acting entity under this License, ‘control’ means direct or indirect ownership of at least fifty percent (50%) of the voting power, capital or other securities of controlled or commonly controlled entity.
 * <p>
 * 1. Grant of Copyright License
 * <p>
 * Subject to the terms and conditions of this License, each Contributor hereby grants to you a perpetual, worldwide, royalty-free, non-exclusive, irrevocable copyright license to reproduce, use, modify, or distribute its Contribution, with modification or not.
 * <p>
 * 2. Grant of Patent License
 * <p>
 * Subject to the terms and conditions of this License, each Contributor hereby grants to you a perpetual, worldwide, royalty-free, non-exclusive, irrevocable (except for revocation under this Section) patent license to make, have made, use, offer for sale, sell, import or otherwise transfer its Contribution, where such patent license is only limited to the patent claims owned or controlled by such Contributor now or in future which will be necessarily infringed by its Contribution alone, or by combination of the Contribution with the Software to which the Contribution was contributed. The patent license shall not apply to any modification of the Contribution, and any other combination which includes the Contribution. If you or your Affiliates directly or indirectly institute patent litigation (including a cross claim or counterclaim in a litigation) or other patent enforcement activities against any individual or entity by alleging that the Software or any Contribution in it infringes patents, then any patent license granted to you under this License for the Software shall terminate as of the date such litigation or activity is filed or taken.
 * <p>
 * 3. No Trademark License
 * <p>
 * No trademark license is granted to use the trade names, trademarks, service marks, or product names of Contributor, except as required to fulfill notice requirements in Section 4.
 * <p>
 * 4. Distribution Restriction
 * <p>
 * You may distribute the Software in any medium with or without modification, whether in source or executable forms, provided that you provide recipients with a copy of this License and retain copyright, patent, trademark and disclaimer statements in the Software.
 * <p>
 * 5. Disclaimer of Warranty and Limitation of Liability
 * <p>
 * THE SOFTWARE AND CONTRIBUTION IN IT ARE PROVIDED WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED. IN NO EVENT SHALL ANY CONTRIBUTOR OR COPYRIGHT HOLDER BE LIABLE TO YOU FOR ANY DAMAGES, INCLUDING, BUT NOT LIMITED TO ANY DIRECT, OR INDIRECT, SPECIAL OR CONSEQUENTIAL DAMAGES ARISING FROM YOUR USE OR INABILITY TO USE THE SOFTWARE OR THE CONTRIBUTION IN IT, NO MATTER HOW IT’S CAUSED OR BASED ON WHICH LEGAL THEORY, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * <p>
 * 6. Language
 * <p>
 * THIS LICENSE IS WRITTEN IN BOTH CHINESE AND ENGLISH, AND THE CHINESE VERSION AND ENGLISH VERSION SHALL HAVE THE SAME LEGAL EFFECT. IN THE CASE OF DIVERGENCE BETWEEN THE CHINESE AND ENGLISH VERSIONS, THE CHINESE VERSION SHALL PREVAIL.
 * <p>
 * END OF THE TERMS AND CONDITIONS
 * <p>
 * How to Apply the Mulan Permissive Software License，Version 2 (Mulan PSL v2) to Your Software
 * <p>
 * To apply the Mulan PSL v2 to your work, for easy identification by recipients, you are suggested to complete following three steps:
 * <p>
 * i Fill in the blanks in following statement, including insert your software name, the year of the first publication of your software, and your name identified as the copyright owner;
 * <p>
 * ii Create a file named “LICENSE” which contains the whole context of this License in the first directory of your software package;
 * <p>
 * iii Attach the statement to the appropriate annotated syntax at the beginning of each source file.
 * <p>
 * <p>
 * Copyright (c) [Year] [name of copyright holder]
 * [Software Name] is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
package com.paul.ons.consumer;

import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.MessageListener;
import com.aliyun.openservices.ons.api.exception.ONSClientException;
import com.aliyun.openservices.ons.api.order.ConsumeOrderContext;
import com.aliyun.openservices.ons.api.order.MessageOrderListener;
import com.aliyun.openservices.ons.api.order.OrderAction;
import com.aliyun.openservices.shade.com.alibaba.fastjson.JSONObject;
import com.paul.ons.util.TagUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * BaseConsumer
 * <p>
 * 基础消费者监听，业务监听对象继承该类完成业务处理，业务正常处理成功提交消息，否则抛出异常进行重试
 * {@link BaseConsumer#consume}
 * {@link Action}
 * </p>
 *
 * @author paul
 * @date 2021-10-11 15:58
 */
public abstract class BaseConsumer implements MessageListener, MessageOrderListener {

    private final Logger logger = LoggerFactory.getLogger(BaseConsumer.class);

    static ConcurrentHashMap<String, Object> mqBeanMap;

    @Resource
    private ApplicationContext applicationContext;

    @Resource
    private ConsumerListenerScan consumerListenerScan;

    private BaseConsumer getBaseConsumer(String topic, String tag) {
        String all = "*";
        String or = "\\|\\|";
        List<Subscriptions> subscriptions = consumerListenerScan.getSubscriptions();
        if (CollectionUtils.isEmpty(mqBeanMap)) {
            synchronized (BaseConsumer.class) {
                if (CollectionUtils.isEmpty(mqBeanMap)) {
                    mqBeanMap = new ConcurrentHashMap<>(subscriptions.size());
                    for (Subscriptions subscription : subscriptions) {
                        Object bean = applicationContext.getBean(subscription.getTargetClass());
                        if (bean instanceof BaseConsumer) {
                            String[] tags = TagUtil.getTag(subscription.getTag()).split(or);
                            for (String splitTag : tags) {
                                mqBeanMap.put(getKey(subscription.getTopic(), splitTag), bean);
                            }
                            mqBeanMap.putIfAbsent(getKey(subscription.getTopic(), all), bean);
                        }
                    }
                }
            }
        }

        Object tagObj = mqBeanMap.get(getKey(topic, tag));
        Object o = tagObj == null ? mqBeanMap.get(getKey(topic, tag = all)) : tagObj;
        if (!(o instanceof BaseConsumer)) {
            throw new ONSClientException("Message consumer " + "'" + o.getClass().getName() + "'" + " creation failed:" + topic + tag);
        }
        return (BaseConsumer) o;
    }

    private String getKey(String topic, String tag) {
        return topic + "_" + tag;
    }

    @Override
    public Action consume(Message message, ConsumeContext context) {
        try {
            doConsumer(message);
        } catch (Exception e) {
            printErrorLog(message, e);
            return Action.ReconsumeLater;
        }
        return Action.CommitMessage;
    }

    @Override
    public OrderAction consume(Message message, ConsumeOrderContext context) {
        try {
            doConsumer(message);
        } catch (Exception e) {
            printErrorLog(message, e);
            return OrderAction.Suspend;
        }
        return OrderAction.Success;
    }

    private void doConsumer(Message message) {
        String topic = message.getTopic();
        String tag = message.getTag();
        String messageBody = new String(message.getBody());
        logger.info("consumer->>>>> topic:{}, tag:{}, messageId:{}, messageBody:{}", topic, tag, message.getMsgID(), messageBody);
        BaseConsumer consumer = getBaseConsumer(topic, tag);
        consumer.consumer(message);
    }

    private void printErrorLog(Message message, Exception e) {
        logger.error("consumer error->>>>> topic:{}, tag:{}, messageId:{}, messageBody:{}", message.getTopic(), message.getTag(), message.getMsgID(), new String(message.getBody()), e);
    }

    /**
     * 消息消费
     *
     * @param message 消息
     */
    protected abstract void consumer(Message message);

    protected <T> T convertMessage(Message message, Class<T> clazz) {
        return JSONObject.parseObject(new String(message.getBody()), clazz);
    }

}
